export const normalizeNews = [
    {
        "id": "1",
        "date": "2018-06-05",
        "title": "Первая статья на React!",
        "text": "Если это заработает будет очень даже круто",
        "comments": ['18', '19']
    },
    {
        "id": "2",
        "date": "2018-06-05",
        "title": "Статья 2",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat volutpat. Curabitur faucibus nulla ac diam egestas mollis.",
        "comments": ['20', '21']
    },
    {
        "id": "3",
        "date": "2018-06-05",
        "title": "Статья 3",
        "text": "Vivamus quis magna egestas nibh convallis sollicitudin. Aenean at lorem rhoncus, hendrerit nisi suscipit, volutpat libero",
        "comments": ['22', '23']
    },
    {
        "id": "4",
        "date": "2018-06-05",
        "title": "Статья 4",
        "text": "Donec ut massa libero. Nulla ut volutpat libero. Nunc at lobortis purus, eget facilisis nisl. ",
        "comments": ['24', '25']
    },
    {
        "id": "5",
        "date": "2018-06-05",
        "title": "Статья 5",
        "text": "Vivamus quis magna egestas nibh convallis sollicitudin. Aenean at lorem rhoncus, hendrerit nisi suscipit, volutpat libero",
        "comments":[]
    }
];
export const normalizeComment = [
    {
        "id": '18',
        "user": 'Иван',
        "text": 'Duis feugiat efficitur leo non ullamcorper.'
    },
    {
        "id": '19',
        "user": 'Ванька',
        "text": 'Praesent at libero nec lorem suscipit placerat non vitae risus. Duis laoreet turpis ut magna faucibus, eu egestas turpis volutpat.'
    },
    {
        "id": '20',
        "user": 'Иван',
        "text": 'Duis laoreet turpis ut magna faucibus, eu egestas turpis volutpat'
    },
    {
        "id": '21',
        "user": 'Ванька',
        "text": 'Vivamus quis magna egestas nibh convallis sollicitudin. '
    },
    {
        "id": '22',
        "user": 'Иван',
        "text": 'Aenean at lorem rhoncus, hendrerit nisi suscipit, volutpat libero'
    },
    {
        "id": '23',
        "user": 'Ванька',
        "text": 'Donec at faucibus elit, et aliquet magna. Aenean placerat leo sit amet felis pulvinar aliquam.'
    },
    {
        "id": '24',
        "user": 'Иван',
        "text": 'Nunc ullamcorper ipsum mi, a gravida nisi mattis vel. Ut at efficitur nisi. Curabitur a elit a est vulputate lobortis vitae ac purus.'
    },
    {
        "id": '25',
        "user": 'Ванька',
        "text": 'Mauris vel aliquam leo. Nullam quam mi, porta vel aliquam at, cursus et sapien.'
    }
];

