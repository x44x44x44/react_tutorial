import {createSelector} from 'reselect'


export const newsSelector = createSelector(
    state => state.newsReducer,
    state => state.newsSelect,
    (newsReducer, newsSelect) => {
        return ( newsReducer.filter((newsElem) =>
            (newsSelect.length === 0 || newsSelect.indexOf(newsElem.id) !== -1)
        ))
    }
);

export const commentSelector = createSelector(
    state => state.commentReducer,
    (state, props) => props.newsElem,
    (commentReducer, newsElem) => {
        return (
            commentReducer.filter((commentElem) =>
                (newsElem.comments.indexOf(commentElem.id) !== -1)
            )
        );
    }
);