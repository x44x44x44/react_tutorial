import {normalizeNews} from '../News-src'
import {DELETE_NEWS,ADD_COMMENT} from '../constants'

export default (newsState = normalizeNews, action) => {
    const {type, payload} = action;
    switch (type) {
        case DELETE_NEWS:
            return newsState.filter(newsElem => newsElem.id !== payload.newsId);
        case ADD_COMMENT:
            const{newCommentElem,newsId} = payload;
            const news = newsState.find((newsElem) => newsElem.id === newsId);
            news.comments.push(newCommentElem.id);
            return newsState;
        default:
    }
    return newsState;
};
