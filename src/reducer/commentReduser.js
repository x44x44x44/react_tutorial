import {normalizeComment} from '../News-src'
import {ADD_COMMENT} from '../constants'

export default (commentState = normalizeComment, action) => {
    const {type, payload} = action;
    switch (type) {
        case ADD_COMMENT:
            const {newCommentElem} = payload;
           return commentState.concat(newCommentElem);
    }
    return commentState;
};
