import {NEWS_SELECT} from '../constants'
const defaultState=[];
export default (newsSelect = defaultState, action) => {
    const {type, payload} = action;
    switch (type) {
        case NEWS_SELECT:
            return payload.chosenNewsId;
        default:
    }
    return newsSelect;
}
