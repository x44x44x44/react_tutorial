import {combineReducers} from 'redux'
import newsReducer from './newsReduser'
import commentReducer from './commentReduser'
import newsSelect from './newsSelect'

export default combineReducers({
    newsReducer,
    commentReducer,
    newsSelect
});