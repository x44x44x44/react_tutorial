import React, {Component as ReactComponent} from 'react'

export default (OriginalComponent) => class ToggleAccardion extends ReactComponent {
    state = {
        openNewsId: null
    };

    render() {
        return (
            <OriginalComponent {...this.props} {...this.state} toggleNews={this.toggleNews}/>
        )
    }
    toggleNews = openNewsId => this.setState({openNewsId:this.state.openNewsId === openNewsId ? null : openNewsId});

}