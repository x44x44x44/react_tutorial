import {DELETE_NEWS,NEWS_SELECT,ADD_COMMENT} from "../constants";
export function deleteNews(newsId){
    return{
        type:DELETE_NEWS,
        payload: { newsId }
    }
}
export function selectNews(chosenNewsId){

    return{
        type:NEWS_SELECT,
        payload: { chosenNewsId }
    }
}
export function addComment(newCommentElem, newsId) {
    return {
        type: ADD_COMMENT,
        payload: {newCommentElem, newsId},
        generateId: true
    }
}