import React, {Component} from 'react'
import NewsList from './NewsList'
import Selector from './Filters/Selector'
import store from '../store'
import {Provider} from 'react-redux'
import 'bootstrap/dist/css/bootstrap.css'


class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className='container'>
                    <h1 className='w-100 text-center jumbotron'>Новости</h1>
                    <Selector/>
                    <NewsList/>
                </div>
            </Provider>
        )
    }
}
export default App;