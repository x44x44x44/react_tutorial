import React, {Component} from 'react';
import PropTypes from 'prop-types'
import Select from 'react-select'
import {connect} from 'react-redux'
import {selectNews} from '../../AC/index.js'

class Selector extends Component {
    static propTypes = {
        //from connect
        newsReducer: PropTypes.array.isRequired,
        selectNews: PropTypes.func.isRequired
    };
    state = {
        selectedOption: ''
    };

    render() {
        const {newsReducer} = this.props;
        const Options = newsReducer.map((newsElem) => (
            {
                value: newsElem.id,
                label: newsElem.title
            }
        ));
        return (
            <form className='mb-4'>
                <Select value={this.state.selectedOption} isMulti onChange={this.changeOption} options={Options}
                        placeholder='Выбрать статью...' noOptionsMessage ={() => 'Статей не найдено'}/>
            </form>
        );
    }

    changeOption = (selectedOption) => {
        const {selectNews} = this.props;
        this.setState({selectedOption});
        const selectedId = selectedOption.map((arrElem) => (
            arrElem.value)
        );
        selectNews(selectedId);
    }
}

export default connect((state) => ({newsReducer: state.newsReducer}),{selectNews})(Selector);
