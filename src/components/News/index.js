import React, {Component} from 'react'
import PropTypes from 'prop-types'
import './style.css'
import Comments from './Comments'

class News extends Component {
    static propTypes = {
        newsElem: PropTypes.shape({
            id: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired
        }).isRequired
    };

    render() {
        const {newsElem, isOpen, onBtnClick} = this.props;
        const body = isOpen ? <div className="b-news__body">{newsElem.text}</div> : '';
        const comments = isOpen ? <Comments newsElem ={newsElem}/> : '';

        return (
            <div className="b-news">
                <button className='b-news__btn' onClick={onBtnClick}>{isOpen ? 'Скрыть' : 'Раскрыть'}</button>
                <button className='b-news__btn' onClick={this.deleteNews}>Удалить</button>
                <h3 className="b-news__title">{newsElem.title}</h3>
                {body}
                <div className="b-news__date">
                    Дата создания: {newsElem.date}
                </div>
                {comments}
            </div>
        )
    }
    deleteNews = () =>{
        const{newsElem,onDeleteBtnClick} = this.props;
        onDeleteBtnClick(newsElem.id);
        console.log('Типо удалилось');
    }
}
export default News;