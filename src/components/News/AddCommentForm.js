import React, {Component} from 'react'
import ToggleOpenClose from '../../decoratiors/ToggleOpenClose'
import './style.css'
import {connect} from 'react-redux'
import {addComment} from '../../AC/index.js'
import randomId from "../../middleware/randomId";


class AddCommentForm extends Component {
    state = {
        commentsName: '',
        commentsText: '',
        commentsNameIsValid: false,
        commentsTextIsValid: false,
        formIsValid: false
    };

    render() {
        const {isOpen, toggleOpen} = this.props;
        const form = isOpen ?
            <form onSubmit={this.submitForm}>
                <input onChange={this.changeState} className={this.state.commentsNameIsValid ? '' : 'error'} type='text'
                       placeholder='Ваше имя' data-input='commentsName' required/>
                <input onChange={this.changeState} className={this.state.commentsTextIsValid ? '' : 'error'}
                       type='text' placeholder='Ваш комментарий' data-input='commentsText' required/>
                <input disabled={!this.state.formIsValid} type='submit' value='Опубликовать комменатрий'
                       className={(this.state.formIsValid ? '' : 'error')}/>
            </form> : '';
        const button = isOpen ? '' : <button onClick={toggleOpen}>Добавить комментарий</button>;
        return (
            <div className='b-add-comment'>
                {form}
                {button}
            </div>
        )
    }

    changeState = (ev) => {
        const inputField = ev.target;
        const stateName = inputField.getAttribute('data-input');
        const inputVal = inputField.value;
        this.setState({
                [stateName]: inputVal
            },
            () => this.validateInput(stateName, inputVal))
    };
    submitForm = (ev) => {
        ev.preventDefault();
        this.setState({
            commentsNameIsValid: false,
            commentsTextIsValid: false,
            formIsValid: false
        }, this.props.toggleOpen());
        const {addComment,newsId} = this.props;

        const newCommentElem = {
            id:(Math.random()).toString(),
            user:this.state.commentsName,
            text:this.state.commentsText
        };
        addComment( newCommentElem, newsId);
        alert('Комментарий к статье успешно добавлен');
    };
    validateInput = (stateName, inputVal) => {
        let nameIsValid = this.state.commentsNameIsValid;
        let textIsValid = this.state.commentsTextIsValid;
        switch (stateName) {
            case 'commentsName':
                nameIsValid = (inputVal.length <= 20 && inputVal.length >= 3 && inputVal.trim() !== '');
                break;
            case 'commentsText':
                textIsValid = (inputVal.length >= 3 && inputVal.trim() !== '');
                break;
            default:
                break
        }
        this.setState({
            commentsNameIsValid: nameIsValid,
            commentsTextIsValid: textIsValid},
            ()=>{
                this.setState({
                    formIsValid: this.state.commentsNameIsValid && this.state.commentsTextIsValid
                })
            }
        )
    };
}
const mapState = (state) =>({
  commentReducer:state.commentReducer
});

export default connect(mapState,{addComment})(ToggleOpenClose(AddCommentForm));