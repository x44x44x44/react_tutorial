import React, {Component} from 'react'
import CommentElem from './CommentElem.js'
import ToggleOpenClose from '../../decoratiors/ToggleOpenClose'
import AddCommentForm from './AddCommentForm'
import {connect} from 'react-redux'
import {commentSelector} from "../../selectors/index";

class Comments extends Component {

    render() {
        const {commentReducer, isOpen, toggleOpen, newsElem} = this.props;
        const commentElem = commentReducer.map((commentElem) =>
            <div key={commentElem.id}>
                {isOpen ? <CommentElem commentElem={commentElem}/> : ''}
            </div>
        );
        if (newsElem.comments.length !== 0) {
            return (
                <div className='b-news__comments'>
                    {commentElem}
                    <button onClick={toggleOpen}>{isOpen ? 'Закрыть комментарии' : 'Открыть комментарии'}</button>
                    <AddCommentForm newsId = {newsElem.id}/>
                </div>
            )
        }
        return (
            <div className='b-news__comments'>
                <div>Комментов еще не завесли</div>
                <AddCommentForm/>
            </div>
        );

    }
}

const mapState = (state, ownProps) => ({
        commentReducer: commentSelector(state, ownProps)
});

export default connect(mapState)(ToggleOpenClose(Comments));


