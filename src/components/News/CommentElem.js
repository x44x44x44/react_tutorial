import React from 'react'
import PropTypes from 'prop-types'


export default function CommentElem(props) {
    const {commentElem} = props;
    return (
        <div className='b-comment'>
            <div className='comments__user-name'>{commentElem.user}</div>
            <div className='comments__text'>{commentElem.text}</div>
        </div>
    )
}
CommentElem.propTypes = {
    commentElem: PropTypes.shape({
            id: PropTypes.string.isRequired,
            user: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired
        }
    ).isRequired
};