import React, {PureComponent} from 'react'
import News from './News/index'
import PropTypes from 'prop-types'
import Accardion from '../decoratiors/Acardion'
import {connect} from 'react-redux'
import {deleteNews} from '../AC/index.js'
import {newsSelector} from "../selectors/index";

class NewsList extends PureComponent {
    static propTypes = {
        //from connect
        newsReducer: PropTypes.array.isRequired,
        newsSelect: PropTypes.array,
        deleteNews: PropTypes.func.isRequired,
        //from Accardion
        openNewsId: PropTypes.string,
        toggleNews: PropTypes.func.isRequired
    };

    render() {
        const {openNewsId, toggleNews, newsReducer, deleteNews} = this.props;
        const newsElements = newsReducer.map((newsElem) =>
            <li className='col-10' key={newsElem.id}>
                <News newsElem={newsElem}
                      isOpen={openNewsId === newsElem.id}
                      onBtnClick={toggleNews.bind(this, newsElem.id)}
                      onDeleteBtnClick={deleteNews}
                />
            </li>
        );
        return (
            <ul className='list-unstyled row justify-content-center'>
                {newsElements}
            </ul>
        )
    }
}

const mapState = (state) => (
    {
        newsReducer: newsSelector(state),
        newsSelect: state.newsSelect
    });

export default connect(mapState, {deleteNews})(Accardion(NewsList));
